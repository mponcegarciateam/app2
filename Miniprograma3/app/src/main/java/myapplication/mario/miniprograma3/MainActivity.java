package myapplication.mario.miniprograma3;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toolbar;


public class MainActivity extends ActionBarActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<Series> items = new ArrayList<>();

        items.add(new Series(R.drawable.juegodetronos, "Juego de Tronos", "Historias de los 7 reinos"));
        items.add(new Series(R.drawable.vikings, "Vikings", "Aventuras de Ragnar Lothbrook"));
        items.add(new Series(R.drawable.loscien, "Los 100", "100 jovenes vuelven a la tierra despues de una guerra nuclear hace 100 años."));
        items.add(new Series(R.drawable.breakingbad, "Breaking bad", "Relata la historia de Walter White despues de recibir la noticia de que tiene cancer."));
        items.add(new Series(R.drawable.shannara, "Las cronicas de Shannara", "Aventuras de fantasia de un shannara y sus compañeros."));
        items.add(new Series(R.drawable.onepiece, "One piece", "Anime que relata las historias de Monkey D. Luffy."));
        items.add(new Series(R.drawable.deathnote, "Deathnote", "Libreta que es usada para matar con solo escribir el nombre."));
        items.add(new Series(R.drawable.shingeki, "Ataque a los titanes", "La civilización humana es aterrada por unos titanes."));

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        adapter = new SeriesAdapter(items);
        recycler.setAdapter(adapter);
    }
}
