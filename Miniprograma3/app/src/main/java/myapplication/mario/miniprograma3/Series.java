package myapplication.mario.miniprograma3;

    public class Series {
        private int imagen;
        private String titulo;
        private String descripcion;

        public Series(int imagen, String titulo, String descripcion) {
            this.imagen = imagen;
            this.titulo = titulo;
            this.descripcion = descripcion;
        }

        public String getTitulo() {
            return titulo;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public int getImagen() {
            return imagen;
        }
    }
